public class Calculadora {


    public static double sumar (double a, double b) {
        double c;
        c = a + b;
        return c;
    }

    public static double restar (double a, double b) {
        double c;
        c = a - b;
        return c;
    }

    public static double multiplicar (double a, double b) {
        return  a * b;
    }

    public static double modulo (double a, double b) {
        return a % b;
    }

    public static double potencia (double a, double b) {
        return Math.pow(a,b);
    }
}
