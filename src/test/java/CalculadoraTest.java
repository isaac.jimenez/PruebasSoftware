import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculadoraTest {

    @Test
    public void sumarPositivosTest(){

        double actual = Calculadora.sumar(2,3);;
        double expected = 4;

        assertEquals(expected, actual);
    }

    @Test
    public void sumarNegativosTest(){

        double actual = Calculadora.sumar(-2,-3);;
        double expected = -4;

        assertEquals(expected, actual);
    }

    @Test
    public void restaTest(){

        double actual = Calculadora.restar(2,2);;
        double expected = 0;

        assertEquals(expected, actual);
    }

    @Test
    public void multiplicarTest(){

        double actual = Calculadora.multiplicar(2,2);;
        double expected = 4;

        assertEquals(expected, actual);
    }

    @Test
    public void moduloTest(){

        double actual = Calculadora.modulo(2,2);;
        double expected = 0;

        assertEquals(expected, actual);
    }

    @Test
    public void potenciaTest(){

        double actual = Calculadora.potencia(5,2);;
        double expected = 25;

        assertEquals(expected, actual);
    }
}
